const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const userManager = require('../db/userManager');
const config = require('../config');
const jwt = require('jsonwebtoken');

const serverSalt = require('../config').serverSalt;

const sha512 = require('./hash');

function initPassport() {
    passport.use(new LocalStrategy({
        session: false
    },
        function (username, password, done) {
            let hash = sha512(password, serverSalt).passwordHash;
            userManager.getUserByLoginAndPasshash(username, hash)
                .then(user => {
                    const payload = {
                        sub: user._id
                    };
                    const token = jwt.sign(payload, config.jwtSecret, {
                        expiresIn: 60 * 60 * 24 * 30
                    });
                    
                    done(user ? null : false, token, user);
                })
                .catch(err => done('no user', null, null));
        }
    ));
}

module.exports = initPassport;