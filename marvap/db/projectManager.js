const mongoose = require('mongoose');
const Project = require('../models/project');

require('./connection');

exports.create = function (proj) {
    let entity = new Project(proj);
    return entity.save();
};

exports.getAll = function (userId) {
    return Project.find({ userId: userId }).exec();
};

exports.getByHeader = function (proj_header) {
    return Project.findOne({ header: proj_header }).exec();
};

exports.update = function (proj_header, proj) {
    let entity = new Project(proj);
    return Project.findOneAndUpdate({ header: proj_header }, entity);
};

exports.remove = function (proj_header, userId) {
    return Project.remove({ header: proj_header, userId: userId });
};

exports.isAlreadyExist = async function (proj_header, userId) {
    let name = await Project.findOne({ header: proj_header, userId: userId }).exec();
    if (!name) return false;
    return true;
}

exports.getCount = function () {
    return Project.count({});
}

exports.searchByName = function (proj_header) {
    return Project.find({header: new RegExp(proj_header, 'i')}).exec();
}
