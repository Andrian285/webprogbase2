const userManager = require('../db/userManager');
const projectManager = require('../db/projectManager');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
const entitiesPerPage = require('../config').entitiesPerPage;

router.get('/userStatistics', checkAuth, async (req, res, next) => {
    let user = res.locals.user;
    if (user.role !== 'admin') return res.status(403).json({message: 'Not Admin'});
    let users = await userManager.getCount();
    let projects = await projectManager.getCount();
    return res.json({
        users: users,
        projects: projects
    });
});

router.get('/userData', checkAuth, async (req, res, next) => {
    let user = res.locals.user;
    if (user.role !== 'admin') return res.status(403).json({message: 'Not Admin'});
    let users = await userManager.getAll();
    let page = req.query.page ? parseInt(req.query.page) : 0;
    return res.json({
        users: users.slice(page * entitiesPerPage, (page + 1) * entitiesPerPage),
        pages: Math.ceil( users.length / entitiesPerPage)
    });
});

module.exports = router;