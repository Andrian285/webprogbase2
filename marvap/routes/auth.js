const passport = require('passport');
const userManager = require('../db/userManager');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
const validation = require('../middleware/validation');
const sha512 = require('../authentication').sha512;
const serverSalt = require('../config').serverSalt; 

router.post('/signup', validation, (req, res) => {
    userManager.getAll()
        .then(datalist => {
            //console.log(req.body.name, req.body.username, req.body.email, req.body.password);
            let user = {
                name: req.body.name,
                username: req.body.username,
                passwordHash: sha512(req.body.password, serverSalt).passwordHash,
                email: req.body.email,
                role: 'user'
            };
            userManager.isAlreadyExist(req.body.username, req.body.email)
                .then(statement => {
                    if (statement) res.status(400).json({ message: 'Username or e-mail is already exist' });
                    else {
                        userManager.create(user)
                            .then(() => {
                                res.json({ message: 'success' });
                            });
                    }
                })
                .catch(err => { console.log(err); res.status(500).json({message: 'Internal Server Error'}); });

        })
        .catch(err => { console.log(err); res.status(500).json({message: 'Internal Server Error'}); });
});

router.post('/login', (req, res, next) => {
    passport.authenticate('local', (err, token, userData) => {
        if (err) return res.status(400).json({
            message: 'Incorrect username or password'
        });
        return res.json({
            token,
            isAdmin: userData.role === 'admin'
            //admin: userData.role === 'admin' ? true : false,
            //user: userData
        });
    })(req, res, next);
});

router.get('/logout',
    checkAuth,
    (req, res) => {
        req.logout();
    });

module.exports = router;