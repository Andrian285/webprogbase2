const userManager = require('../db/userManager');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
const validation = require('../middleware/validation');

router.get('/', checkAuth, (req, res, next) => {
    let user = res.locals.user;
    return res.json({
        name: user.name,
        email: user.email,
        username: user.username,
        photo: user.photo,
        isAdmin: user.role === 'admin'
    });
});

router.post('/upload', checkAuth, validation, async (req, res) => {
    let user = res.locals.user;
    try {
        if (req.body.name && req.body.username && req.body.email) {
            let statement = await userManager.isAlreadyExist(req.body.username === user.username ? '' : req.body.username, 
            req.body.email === user.email ? '' : req.body.email);
            if (statement) return res.status(400).json({ message: 'Username or e-mail is already exist' });

            await userManager.update(user._id, {
                photo: user.photo,
                name: req.body.name,
                username: req.body.username,
                passwordHash: user.passwordHash,
                email: req.body.email,
                role: user.role === 'user' ? 'user' : 'admin',
                _id: user._id,
                id: 0
            });

            if (req.body.avatar) {
                await userManager.updatePhoto(user._id, req.body.avatar);
                res.json({ message: 'success' });
            }
            else res.json({ message: 'success' });
        }
        else {
            if (req.body.avatar) {
                await userManager.updatePhoto(user._id, req.body.avatar);
                res.json({ message: 'success' });
            }
            else res.status(400).json({ message: 'Incorrect profile data' });
        }
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

module.exports = router;