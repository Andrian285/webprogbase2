function initRoutes(app) {
    const authRouter = require('./auth');
    app.use("/auth", authRouter);
    
    const profileRouter = require('./profile');
    app.use("/user/profile", profileRouter);
    
    const projectsRouter = require('./projects');
    app.use("/user/projects", projectsRouter);

    const adminRouter = require('./admin');
    app.use("/admin", adminRouter);

    app.post("/*", (req, res) => {
        res.status(404).json({message: 'Not Found'});
    });
}

module.exports = initRoutes;