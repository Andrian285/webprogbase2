const passport = require('passport');
const userManager = require('../db/userManager');
const projectManager = require('../db/projectManager');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
const entitiesPerPage = require('../config').entitiesPerPage; 

router.get('/', checkAuth, async (req, res) => {
    let user = res.locals.user;
    try {
        let page = req.query.page ? parseInt(req.query.page) : 0;
        if (req.query.search) {
            if (!req.query.search) return res.status(400).json({message: 'search identifier is null'});
            let projects = await projectManager.searchByName(req.query.search, user._id);
            if (!projects) return res.status(404).json({message: 'no projects found'});
            return res.json({
                projects: projects.slice(page * entitiesPerPage, (page + 1) * entitiesPerPage),
                pages: Math.ceil( projects.length / entitiesPerPage)
            });
        }

        let projs = await projectManager.getAll(user._id);
        projs.sort((a, b) => b.lastEdited - a.lastEdited);
        //console.log(projs);
        let projects = [];
        for (proj of projs) projects.push({
            header: proj.header,
            meta: proj.meta,
            description: proj.description
        });
        res.json({
            projects: projects.slice(page * entitiesPerPage, (page + 1) * entitiesPerPage),
            pages: Math.ceil( projects.length / entitiesPerPage)
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

router.post('/save', checkAuth, async (req, res) => {
    let user = res.locals.user;
    if (!req.body.header) return res.status(400).json({ message: 'Project name is null' });
    let exist = await projectManager.isAlreadyExist(req.body.header, user._id);
    if (!exist) {
        let project = {
            header: req.body.header,
            meta: req.body.meta === 'null' ? '' : req.body.meta,
            description: req.body.description === 'null' ? '' : req.body.description,
            nodes: [],
            edges: [],
            userId: user._id,
            lastEdited: Date.now()
        };

        try {
            await projectManager.create(project);
            res.json({ message: 'success' });
        } catch (err) {
            console.log(err);
            res.res.status(500).json({ message: 'Internal Server Error' });
        }
    }
    else {
        res.status(400).json({ message: 'Project with such name is already exist' });
    }
});

router.post('/remove', checkAuth, async (req, res) => {
    let user = res.locals.user;
    let exist = await projectManager.isAlreadyExist(req.body.name, user._id);
    if (exist) {
        try {
            await projectManager.remove(req.body.name, user._id);
            res.json({ message: 'success' });
        } catch (err) {
            console.log(err);
            res.res.status(500).json({ message: 'Internal Server Error' });
        }
    }
    else {
        res.status(400).json({ message: 'Project with such name does not exist' });
    }
});

router.get('/:id', checkAuth, async (req, res) => {
    let user = res.locals.user;
    if (!req.params.id) return res.status(400).json({ message: 'Project name is null' });
    if (req.body.nodes || req.body.edges) return res.status(400).json({ message: 'Nodes or Edges are null' });

    try {
        let project = await projectManager.getByHeader(req.params.id);
        if (!project) return res.status(400).json({message: 'Invalid project name'});
        res.json({ project });
    } catch (err) {
        console.log(err);
        res.res.status(500).json({ message: 'Internal Server Error' });
    }
});

router.post('/:id/save', checkAuth, async (req, res) => {
    let user = res.locals.user;
    if (!req.params.id) return res.status(400).json({ message: 'Project name is null' });
    if (!req.body.nodes || !req.body.edges) return res.status(400).json({ message: 'Nodes or Edges are null' });

    try {
        let project = await projectManager.getByHeader(req.params.id);
        if (!project) return res.status(400).json({message: 'Invalid project name'});
        let newNodes = JSON.parse(req.body.nodes), newEdges = JSON.parse(req.body.edges);
        if (newNodes.find(x => !x.label)) return res.status(400).json({message: 'some node does not contain data'});
        for (let node of newNodes) {
            let projectId = project.nodes.indexOf(project.nodes.find(x => x.id === node.id));
            if (projectId === -1) continue;
            if (!node.image && project.nodes[projectId].image) node.image = project.nodes[projectId].image;
        }
        await projectManager.update(project.header, {
            header: project.header,
            meta: project.meta,
            description: project.description,
            nodes: newNodes,
            edges: newEdges,
            userId: user._id,
            lastEdited: Date.now(),
            _id: project._id
        });
        res.json({ message: 'success' });
    } catch (err) {
        console.log(err);
        res.res.status(500).json({ message: 'Internal Server Error' });
    }
});

module.exports = router;