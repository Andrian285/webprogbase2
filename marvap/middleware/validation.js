const jwt = require('jsonwebtoken');
const config = require('../config');
const userManager = require('../db/userManager');
const emailValidator = require('email-validator');

function validation(req, res, next) {
    function badrequest() {
        res.status(400).json({message: 'Bad Request'});
    }

    if (!req.body.username || !req.body.name || !req.body.email || (!res.locals.user && !req.body.password)) return badrequest();

    if (!/^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(req.body.name) ||
        req.body.username.length < 3 ||
        !emailValidator.validate(req.body.email)) return badrequest();

    if (!res.locals.user && req.body.password.length < 8) return badrequest();
    
    next();
}

module.exports = validation;