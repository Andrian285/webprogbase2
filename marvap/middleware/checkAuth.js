const jwt = require('jsonwebtoken');
const config = require('../config');
const userManager = require('../db/userManager');

function checkAuth(req, res, next) {
    function unauthorized() {
        res.status(401).json({message: 'Unauthorized'});
    }

    if (!req.headers.token) {
        return unauthorized();
    }

    const token = req.headers.token;

    // decode the token using a secret key-phrase
    return jwt.verify(token, config.jwtSecret, (err, decoded) => {
        if (err) { return unauthorized(); }

        const userId = decoded.sub;

        userManager.getById(userId)
            .then(user => {
                if (!user) return unauthorized();
                res.locals.user = user;
                next();
            })
            .catch(() => { return unauthorized(); })
    });
}

module.exports = checkAuth;