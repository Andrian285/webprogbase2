const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const app = express();
const passport = require('passport');
const path = require("path");
require('dotenv').config();

app.use(bodyParser.urlencoded({
    limit: '900kb',
    extended: false
}));
app.use(busboyBodyParser({ limit: '5mb' }));
app.use(passport.initialize());

app.use(express.static(path.join(__dirname + '/client/', 'build')));

require('./authentication').init();

require('./routes').init(app);

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

app.listen(process.env.PORT, function () {
    console.log('server is listening at %s', process.env.PORT);
});
