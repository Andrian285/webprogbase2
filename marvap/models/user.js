const mongoose = require('mongoose');

let User = mongoose.model('User', {
    photo: String,
	name: String, 
	username: String,
	passwordHash: String,
    email: String,
    role: String,
    id: Number
});

module.exports = User;