const mongoose = require('mongoose');

let Project = mongoose.model('Project', {
    header: String,
    meta: String,
    description: String,
    nodes: [{
        id: Number,
        label: String,
        x: Number,
        y: Number,
        image: String
    }],
    edges: [{
        from: Number,
        to: Number
    }],
    lastEdited: Date,
    userId: mongoose.Schema.Types.ObjectId
});

module.exports = Project;