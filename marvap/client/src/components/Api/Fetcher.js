export default async function sendData(body, link, method, token, page) {
    try {
        let headers = {'Content-Type': 'application/x-www-form-urlencoded'};
        if (token) headers.token = token;
        if (page) headers.page = page;

        let obj = {
            method: method,
            headers: headers,
        };

        if (method.toLowerCase() === 'post') obj.body = body;

        let response = await fetch(link, obj);
        //console.log(response);
        //let responseData = await response.json();

        //console.log(responseData);
        return response;
    } catch (err) {
        console.log(err);
        return null;
    }

}