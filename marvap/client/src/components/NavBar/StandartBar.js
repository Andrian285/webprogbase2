import React from 'react';
import logo from '../../images/navbarLogo.png';
import { Menu, Image } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import '../../stylesheets/index.css';

export default class StandartBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: this.props.page.path
        };
    }

    handleItemClick = (e, { name, page }) => this.setState({ activeItem: name, redirect: page });

    render() {
        if (this.state.redirect !== this.props.page.path) return (<Redirect to={this.state.redirect} />);
        return (
            <Menu inverted attached='top'>
                <Menu.Item
                    disabled
                    name='logo'
                >
                    <Image src={logo} className='nbLogo' />
                </Menu.Item>

                <Menu.Item
                    name='home'
                    page='/'
                    active={this.props.page.path === '/'}
                    onClick={this.handleItemClick}
                >
                    Home
                </Menu.Item>

                {this.props.restMenu()}
            </Menu>
        );
    }
}