import React from 'react';
import { Menu, Image, Icon } from 'semantic-ui-react';
import logo from '../../images/navbarLogo.png';
import { Redirect } from 'react-router-dom';

export default class MobileBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: this.props.page.path,
            openVerticalMenu: false
        }
    }

    handleItemClick = (e, { name, page }) => this.setState({ activeItem: name, redirect: page });

    renderMobile() {
        if (this.state.openVerticalMenu) return (
            <Menu inverted stackable attached='bottom'>
                <Menu.Item
                    name='home'
                    page='/'
                    active={this.props.page.path === '/'}
                    onClick={this.handleItemClick}
                >
                    Home
                </Menu.Item>

                {this.props.restMenu()}
            </Menu>
        );
    }

    render() {
        if (this.state.redirect !== this.props.page.path) return (<Redirect to={this.state.redirect} />);
        return (
            <div>
                <Menu inverted attached='top'>
                    <Menu.Item
                        disabled
                        name='logo'
                    >
                        <Image src={logo} className='nbLogo' />
                    </Menu.Item>

                    <Menu.Menu position='right'>
                        <Menu.Item
                            onClick={() => this.setState({ openVerticalMenu: !this.state.openVerticalMenu })}
                            name='logo'
                        >
                            <Icon name='sidebar' size='big' />
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                {this.renderMobile()}
            </div>
        );
    }
}