import React from 'react';
import '../../stylesheets/index.css';
import { Menu, Modal, Segment, Responsive } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { Redirect } from 'react-router-dom';
import Auth from '../Auth/Auth';
import Warning from '../Warning/warning';
import MobileBar from './MobileBar';
import StandartBard from './StandartBar';

export default class MainBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            redirect: this.props.page.path
        };
    }

    show = dimmer => () => this.setState({ dimmer, open: true });
    close = () => this.setState({ open: false });
    handleItemClick = (e, { name, page }) => this.setState({ activeItem: name, redirect: page });

    restMenu = () => {
        return (
            <Menu.Menu position='right'>
                <Menu.Item
                    name='sign in'
                    active={this.props.path === '/login'}
                    onClick={this.show('blurring')}
                >
                    Sign In

                </Menu.Item>
                    <Menu.Item
                        name='sign up'
                        page='/SignUp'
                        active={this.props.page.path === '/signup'}
                        onClick={this.handleItemClick}
                    >
                        Sign Up
                </Menu.Item>
            </Menu.Menu>
        );
    }

    render() {
        const { open, dimmer } = this.state;
        if (this.state.redirect !== this.props.page.path) return (<Redirect to={this.state.redirect} />);
        return (
            <div>
                <Responsive minWidth={600} >
                    <StandartBard page={this.props.page} restMenu={this.restMenu} />
                </Responsive>
                <Responsive maxWidth={599} >
                    <MobileBar page={this.props.page} restMenu={this.restMenu} />
                </Responsive>

                <Modal dimmer={dimmer} open={open} onClose={this.close}>
                    <Warning warning={this.state.warning} />
                    <Modal.Header>Sign In</Modal.Header>
                    <Segment>
                        <Auth username password submit sendTo='/auth/login' page={this.props.page} />
                    </Segment>
                </Modal>
            </div>
        )
    }
}