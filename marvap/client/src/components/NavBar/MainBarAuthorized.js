import React from 'react';
import '../../stylesheets/index.css';
import { Menu, Dropdown, Responsive } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { Redirect } from 'react-router-dom';
import LocalStorageManager from '../../components/Auth/LocalStorageManager';
import MobileBar from './MobileBar';
import StandartBard from './StandartBar';
import sendData from '../Api/Fetcher';
import MainBar from './MainBar';
import { setUsername, logout } from '../../actions/actions';
import { connect } from 'react-redux';

class MainBarAuthorized extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: this.props.page.path,
            resetReduxData: false,
            fakeUnauthorizedModal: false,
            isAdmin: false
        };
    }

    handleItemClick = (e, { name, page }) => this.setState({ activeItem: name, redirect: page });

    logout() {
        if (LocalStorageManager.isUserAuthenticated()) {
            LocalStorageManager.deauthenticateUser();
            if (this.props.page.path !== '/') this.setState({redirect: '/'});
            else this.setState({fakeUnauthorizedModal: true});
            this.setState({resetReduxData: true});
            //this.setState({ refresh: true }); 
        }
    }

    async componentDidMount() {
        let data = await sendData(null, `/user/profile`, 'GET', LocalStorageManager.getToken());
        let responseData = await data.json();
        if (data.status === 401 || data.status === 403) this.logout();
        
        if (this.props.username !== responseData.username) this.props.setUsername(responseData.username);
        if (!responseData.isAdmin && LocalStorageManager.isAdmin()) LocalStorageManager.setIsAdmin('false');
        if (responseData.isAdmin) this.setState({isAdmin: true});
        //console.log(LocalStorageManager.isAdmin());
    }

    adminPage() {
        if (this.state.isAdmin) {
            return (
                <Dropdown.Item
                    name='statistics'
                    page='/statistics'
                    onClick={this.handleItemClick}
                >
                    Statistics
                </Dropdown.Item>
            );
        }
    }

    dropDownMenu = () => {
        return (
            <Menu.Menu position='right'>
                <Dropdown loading={this.props.username === undefined} text={this.props.username ? this.props.username : ''} simple item>
                    <Dropdown.Menu>
                        <Dropdown.Item
                            name='profile'
                            page='/profile'
                            onClick={this.handleItemClick}
                        >
                            Profile
                </Dropdown.Item>
                        <Dropdown.Item
                            name='projects'
                            page='/projects'
                            onClick={this.handleItemClick}
                        >
                            Projects
                </Dropdown.Item>
                        {this.adminPage()}
                        <Dropdown.Item
                            name='logout'
                            onClick={() => this.logout()}
                        >
                            Logout
                </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Menu.Menu>
        );
    }

    componentWillMount() {
        if (this.state.resetReduxData) this.props.logout();
    }

    render() {
        if (this.state.redirect !== this.props.page.path) return (<Redirect to={this.state.redirect} />);
        if (this.state.fakeUnauthorizedModal) return <MainBar page={this.props.page} />;
        return (
            <div>
                <Responsive minWidth={600} >
                    <StandartBard page={this.props.page} restMenu={this.dropDownMenu} />
                </Responsive>
                <Responsive maxWidth={599} >
                    <MobileBar page={this.props.page} restMenu={this.dropDownMenu} />
                </Responsive>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    username: state.username
});

const mapDispatchToProps = {
    setUsername,
    logout
};
const MainBarAuthorizedComponent = connect(mapStateToProps, mapDispatchToProps)(MainBarAuthorized);

export default MainBarAuthorizedComponent;