import React from 'react';
import validator from 'email-validator';
import {Form, Input, Icon, Button, Label, Transition} from 'semantic-ui-react';
import Warning from '../Warning/warning';
import LocalStorageManager from './LocalStorageManager';
import {Redirect} from 'react-router-dom';
import sendData from '../Api/Fetcher';

export default class Auth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            confirm: '',
            username: '',
            visibleNameError: false,
            visibleEmailError: false,
            visiblePassError: false,
            visibleConfirmError: false,
            visibleUsernameError: false,
            nameError: 'Incorrect Full Name: you should enter your name and surname form capital letters',
            usernameError: 'Incorrect Username: must be at least 3 characters',
            emailError: 'Incorrect e-mail',
            passError: 'Incorrect password: must be at least 8 characters',
            confirmError: 'The password and its confirm are not the same',
            formValid: false,
            warning: null,
            refresh: false,
            redirect: false,
            loading: false
        };
        this.handleChange = this.handleChange.bind(this);
    }

    async processReceivedData(data) {
        //console.log(data);
        this.setState({loading: false});
        let responseData = await data.json();
        let status = [500, 400, 401, 403];
        if (!status.includes(data.status)) {
            if (responseData.token) LocalStorageManager.authenticateUser(responseData.token, responseData.isAdmin);
            if (this.props.page.path !== '/') {
                this.setState({redirect: true});
            }
            else this.setState({refresh: true});
        }
        else {
            this.setState({warning: responseData.message});
            this.setState({formValid: false});
        }
    }

    async sendData() {
        let data = ``;
        if (this.props.name)
            data += `name=${this.state.name}`;
        if (this.props.username)
            data += `&username=${this.state.username}`;
        if (this.props.password)
            data += `&password=${this.state.password}`;
        if (this.props.email)
            data += `&email=${this.state.email}`;

        let responseData = await sendData(data, this.props.sendTo, 'POST');
        this.processReceivedData(responseData);
    }

    setFormValid() {
        this.setState({loading: true});
        let name = true, username = true, pass = true, confirm = true, email = true;
        if (this.props.name)
            name = this.toggleError('visibleNameError', this.state.name, this.state.visibleNameError, /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(this.state.name));
        if (this.props.username)
            username = this.toggleError('visibleUsernameError', this.state.username, this.state.visibleUsernameError, this.state.username.length >= 3);
        if (this.props.password)
            pass = this.toggleError('visiblePassError', this.state.password, this.state.visiblePassError, this.state.password.length >= 8);
        if (this.props.confirm)
            confirm = pass === true ? this.toggleError('visibleConfirmError', this.state.confirm, this.state.visibleConfirmError, this.state.confirm === this.state.password) : false;
        if (this.props.email)
            email = this.toggleError('visibleEmailError', this.state.email, this.state.visibleEmailError, validator.validate(this.state.email));
        if (!this.state.formValid &&
            name && username && pass && confirm && email) {
            this.sendData();
            this.setState({formValid: true});
        } else this.setState({loading: false});
    }

    toggleError(e, field, fieldErr, additionalRequire) {
        if (!field || !additionalRequire) {
            if (!fieldErr) this.setState({[e]: !this.state[e]});
            return false;
        }
        if (fieldErr) this.setState({[e]: !this.state[e]});
        return true;
    }

    handleChange(e) {
        const validIdDetector = e.target.name;
        this.setState({[e.target.name]: e.target.value}, () => {
            if (validIdDetector === 'name')
                this.toggleError('visibleNameError', this.state.name, this.state.visibleNameError, /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(this.state.name));
            else if (validIdDetector === 'username')
                this.toggleError('visibleUsernameError', this.state.username, this.state.visibleUsernameError, this.state.username.length >= 3);
            else if (validIdDetector === 'password' || validIdDetector === 'confirm') {
                let confAllowed = this.toggleError('visiblePassError', this.state.password, this.state.visiblePassError, this.state.password.length >= 8);
                if (confAllowed) this.toggleError('visibleConfirmError', this.state.confirm, this.state.visibleConfirmError, this.state.confirm === this.state.password);
            }
            else if (validIdDetector === 'email')
                this.toggleError('visibleEmailError', this.state.email, this.state.visibleEmailError, validator.validate(this.state.email));
        });
    }

    renderNameField() {
        if (this.props.name) return (
            <Form.Field>
                <label>Full name</label>
                <Input placeholder='Full name' onChange={this.handleChange} name={'name'}/>
                <Transition visible={this.state.visibleNameError} animation='scale' duration={500}>
                    <Label basic color='red' pointing>{this.state.nameError}</Label>
                </Transition>
            </Form.Field>
        );
    }

    renderUsernameField() {
        if (this.props.username) return (
            <Form.Field>
                <label>Username</label>
                <Input placeholder='Username' onChange={this.handleChange} name={'username'}/>
                <Transition visible={this.state.visibleUsernameError} animation='scale' duration={500}>
                    <Label basic color='red' pointing>{this.state.usernameError}</Label>
                </Transition>
            </Form.Field>
        );
    }

    renderPassField() {
        if (this.props.password) return (
            <Form.Field>
                <label>Password</label>
                <Input type='password' onChange={this.handleChange} name={'password'}/>
                <Transition visible={this.state.visiblePassError} animation='scale' duration={500}>
                    <Label basic color='red' pointing>{this.state.passError}</Label>
                </Transition>
            </Form.Field>
        );
    }

    renderConfirmField() {
        if (this.props.confirm) return (
            <Form.Field>
                <label>Confirm Password</label>
                <Input type='password' onChange={this.handleChange} name={'confirm'}/>
                <Transition visible={this.state.visibleConfirmError} animation='scale'
                            duration={500}>
                    <Label basic color='red' pointing>{this.state.confirmError}</Label>
                </Transition>
            </Form.Field>
        );
    }

    renderEmailField() {
        if (this.props.email) return (
            <Form.Field>
                <label>E-mail</label>
                <Input placeholder='E-mail' onChange={this.handleChange} name={'email'}/>
                <Transition visible={this.state.visibleEmailError} animation='scale' duration={500}>
                    <Label basic color='red' pointing>{this.state.emailError}</Label>
                </Transition>
            </Form.Field>
        );
    }

    render() {
        if (this.state.refresh) {
            window.location.reload();
        }
        if (this.state.redirect) {
            return (<Redirect to='/'/>);
        }
        return (
            <div>
                <Warning warning={this.state.warning}/>
                <Form>
                    {this.renderNameField()}
                    {this.renderUsernameField()}

                    <Form.Group widths='equal'>
                        {this.renderPassField()}
                        {this.renderConfirmField()}
                    </Form.Group>

                    {this.renderEmailField()}

                    <Form.Field>
                        <Button positive animated onClick={() => this.setFormValid()} loading={this.state.loading}>
                            <Button.Content visible>Submit</Button.Content>
                            <Button.Content hidden>
                                <Icon name='right arrow'/>
                            </Button.Content>
                        </Button>
                    </Form.Field>
                </Form>
            </div>
        );
    }
}