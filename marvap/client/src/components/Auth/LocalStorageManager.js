class LocalStorageManager {

    static authenticateUser(token, admin) {
        localStorage.setItem('token', token);
        localStorage.setItem('admin', admin);
    }

    static isUserAuthenticated() {
        return localStorage.getItem('token') !== null;
    }

    static deauthenticateUser() {
        localStorage.removeItem('token');
        localStorage.removeItem('admin');
    }

    static getToken() {
        return localStorage.getItem('token');
    }

    static isAdmin() {
        return localStorage.getItem('admin') === 'true';
    }

    static setIsAdmin(statement) {
        localStorage.setItem('admin', statement);
    }

}

export default LocalStorageManager;