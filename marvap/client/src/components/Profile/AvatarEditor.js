import React from 'react';
import { Image, Label, Transition, Dimmer, Card, Loader, Input, Segment } from 'semantic-ui-react';
import Avatar from '../../images/avatar.png';

export default class AvatarEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibleAvatarError: false,
            avatarError: 'Invalid image size (maximum is 900 KB) or file format (correct is jpeg/png)'
        };
        this.loadPhoto = this.loadPhoto.bind(this);
    }

    async loadPhoto(evt) {
        const reader = new FileReader();
        let photo = evt.target.files[0];
        reader.onloadend = () => {
            if (photo.size > 900000 || (photo.type !== 'image/jpeg' && photo.type !== 'image/png')) {
                this.setState({ visibleAvatarError: true });
                return;
            }
            else {
                if (this.state.visibleAvatarError)
                    this.setState({ visibleAvatarError: false });
                this.props.onLoad(reader.result);
            }
        };
        try {
            reader.readAsDataURL(photo);
        } catch (e) {
            //loading canceled
        }
    }

    render() {
        return (
            <Card centered>
                <Dimmer active={this.props.loading}>
                    <Loader />
                </Dimmer>
                <Image src={this.props.avatar === null ? Avatar : this.props.avatar} className='img' />
                <Transition visible={this.state.visibleAvatarError} animation='scale'
                    duration={500}>
                    <Label basic color='red' pointing>{this.state.avatarError}</Label>
                </Transition>

                <Segment>
                    <Input type='file' fluid onChange={this.loadPhoto} />
                </Segment>
            </Card>
        );
    }
}