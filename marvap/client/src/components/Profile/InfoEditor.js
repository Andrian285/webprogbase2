import React from 'react';
import validator from 'email-validator';
import {Header, Label, Transition, Button} from 'semantic-ui-react';

export default class InfoEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            uname: null,
            email: null,
            showSave: false,
            redirect: false,
            visibleNameError: false,
            visibleUsernameError: false,
            visibleEmailError: false,
            nameError: 'Incorrect Full Name: you should enter your name and surname form capital letters',
            unameError: 'Incorrect Username: must be at least 3 characters',
            emailError: 'Incorrect e-mail',
            saveLoading: false
        };
        this.handleChange = this.handleChange.bind(this);
    }

    checkFormValid() {
        let name = true, uname = true, email = true;
        if (this.state.name !== null)
            name = this.toggleError('visibleNameError', this.state.name, this.state.visibleNameError, /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(this.state.name));
        if (this.state.uname !== null)
            uname = this.toggleError('visibleUsernameError', this.state.uname, this.state.visibleUsernameError, this.state.uname.length >= 3);
        if (this.state.email !== null)
            email = this.toggleError('visibleEmailError', this.state.email, this.state.visibleEmailError, validator.validate(this.state.email));
        if (name && uname && email) {
            this.props.setStateSaveLoading(true);
                this.props.sendData(`name=${this.state.name ? this.state.name : this.props.name}`+
                `&username=${this.state.uname ? this.state.uname : this.props.uname}`+
                `&email=${this.state.email ? this.state.email : this.props.email}`);
        }
    }

    toggleError(e, field, fieldErr, additionalRequire) {
        if (!field || !additionalRequire) {
            if (!fieldErr) this.setState({[e]: !this.state[e]});
            return false;
        }
        if (fieldErr) this.setState({[e]: !this.state[e]});
        return true;
    }

    handleChange(e) {
        if (!this.state.showSave) this.setState({showSave: true});
        this.setState({infoChanged: true});
        const validIDdetector = e.target.name;
        this.setState({[e.target.name]: e.target.value}, () => {
            if (validIDdetector === 'name')
                this.toggleError('visibleNameError', this.state.name, this.state.visibleNameError, /^[A-Z]+[a-z]+\s+[A-Z]+[a-z]+$/.test(this.state.name));
            else if (validIDdetector === 'uname')
                this.toggleError('visibleUsernameError', this.state.uname, this.state.visibleUsernameError, this.state.uname.length >= 3);
            else this.toggleError('visibleEmailError', this.state.email, this.state.visibleEmailError, validator.validate(this.state.email));
        });
    }

    save() {
        if (this.state.showSave) {
            this.checkFormValid();
        }
        else {
            this.props.setStateSaveLoading(true);
            this.props.sendData();
        };
    }

    saveRender() {
        if (this.props.showSave || this.state.showSave) return (
            <Button positive fluid loading={this.props.saveIsLoading} onClick={() => this.save()}>Save</Button>
        );
    }

    editRender() {
        if (!this.state.fieldsEditable) return (
            <div>
                <Button floated='right'
                        onClick={() => this.setState({fieldsEditable: true})}>Edit</Button><br/><br/><br/>
            </div>
        );
    }

    render() {
        return (
            <div className="ui form">
                <div className="inline field">
                    <div className="ui stackable two column grid">
                        <div className="five wide column middle aligned content"><Header
                            size='small'>Full name</Header></div>
                        <div className="eleven wide column"><input
                            placeholder={this.props.name === null ? '' : this.props.name}
                            type="text"
                            disabled={!this.state.fieldsEditable} onChange={this.handleChange}
                            name={'name'}
                            className='visiblePlaceholder' maxLength='30'/></div>
                        <Transition visible={this.state.visibleNameError} animation='scale'
                                    duration={500}>
                            <Label basic color='red' pointing>{this.state.nameError}</Label>
                        </Transition>
                    </div>
                </div>

                <div className="inline field">
                    <div className="ui stackable two column grid">
                        <div className="five wide column middle aligned content"><Header
                            size='small'>Username</Header></div>
                        <div className="eleven wide column"><input
                            placeholder={this.props.uname === null ? '' : this.props.uname}
                            type="text"
                            disabled={!this.state.fieldsEditable} onChange={this.handleChange}
                            name={'uname'}
                            className='visiblePlaceholder' maxLength='30'/></div>
                        <Transition visible={this.state.visibleUsernameError} animation='scale'
                                    duration={500}>
                            <Label basic color='red' pointing>{this.state.unameError}</Label>
                        </Transition>
                    </div>
                </div>

                <div className="inline field">
                    <div className="ui stackable two column grid">
                        <div className="five wide column middle aligned content"><Header
                            size='small'>E-mail</Header></div>
                        <div className="eleven wide column"><input
                            placeholder={this.props.email === null ? '' : this.props.email}
                            type="text"
                            disabled={!this.state.fieldsEditable} onChange={this.handleChange}
                            name={'email'}
                            className='visiblePlaceholder' maxLength='30'/></div>
                        <Transition visible={this.state.visibleEmailError} animation='scale'
                                    duration={500}>
                            <Label basic color='red' pointing>{this.state.emailError}</Label>
                        </Transition>
                    </div>
                </div>

                {this.editRender()}
                {this.saveRender()}
            </div>
        );
    }
}