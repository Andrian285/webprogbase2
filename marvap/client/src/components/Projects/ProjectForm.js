import React from 'react';
import {Form, Icon, Button, Label, Transition, TextArea, Container} from 'semantic-ui-react';
import LocalStorageManager from '../Auth/LocalStorageManager';
import sendData from '../Api/Fetcher';

export default class ProjectForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tmpHeader: null,
            tmpMeta: null,
            tmpDescription: null,
            visibleHeaderError: false,
            headerError: 'This field is required',
            loadingSave: false
        };
        this.handleChange = this.handleChange.bind(this);
    }

    async addProject() {
        this.setState({loadingSave: true}, async () => {
            let body = `header=${this.state.tmpHeader}&meta=${this.state.tmpMeta}&description=${this.state.tmpDescription}`;
            let data = await sendData(body, `/user/projects/save`, 'POST', LocalStorageManager.getToken());
            this.setState({ loadingSave: false });
            this.props.addProjectOnClient(data, this.state.tmpHeader, this.state.tmpMeta, this.state.tmpDescription);            
        });
    }

    checkFormValid() {
        //console.log(this.state.tmpMeta, this.state.tmpDescription);
        let header = this.toggleError('visibleHeaderError', this.state.tmpHeader, this.state.visibleHeaderError);
        if (header) this.addProject();
    }

    toggleError(e, field, fieldErr) {
        if (!field) {
            if (!fieldErr) this.setState({[e]: !this.state[e]});
            return false;
        }
        if (fieldErr) this.setState({[e]: !this.state[e]});
        return true;
    }

    handleChange(e) {
        const validIdDetector = e.target.name;
        this.setState({[e.target.name]: e.target.value}, () => {
            if (validIdDetector === 'tmpHeader')
                this.toggleError('visibleHeaderError', this.state.tmpHeader, this.state.visibleHeaderError);
        });
    }

    render() {
        return (
            <Form>
                <Container textAlign='center'>
                    <Form.Field>
                        <label>Name</label>
                        <input placeholder='Header' onChange={this.handleChange} name={'tmpHeader'}
                               maxLength="10"/>
                        <Transition visible={this.state.visibleHeaderError} animation='scale'
                                    duration={500}>
                            <Label basic color='red' pointing>{this.state.headerError}</Label>
                        </Transition>
                    </Form.Field>

                    <Form.Field>
                        <label>Meta</label>
                        <input placeholder='Meta' onChange={this.handleChange} name={'tmpMeta'}
                               maxLength="10"/>
                    </Form.Field>

                    <Form.Field>
                        <label>Description</label>
                        <TextArea autoHeight placeholder='Write something about your project'
                                  onChange={this.handleChange}
                                  name={'tmpDescription'} maxLength="40"/>
                    </Form.Field>

                    <Form.Field>
                        <Button positive loading={this.state.loadingSave} animated={!this.state.loadingSave} onClick={() => this.checkFormValid()}>
                            <Button.Content visible>Submit</Button.Content>
                            <Button.Content hidden>
                                <Icon name='right arrow'/>
                            </Button.Content>
                        </Button>
                    </Form.Field>
                </Container>
            </Form>
        );
    }
}