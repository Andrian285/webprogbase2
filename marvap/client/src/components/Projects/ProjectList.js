import React from 'react';
import { Card, Button, Segment, Dimmer, Loader, Header, Responsive, Container } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';

export default class ProjectList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: null
        };
    }

    showProjects() {
        let projs = [];
        for (let i = 0; i < this.props.projects.length; i++)
            projs.push(
                <Card key={this.props.projects[i].header}>
                    <Card.Content>
                        <Card.Header>{this.props.projects[i].header}</Card.Header>
                        <Card.Meta>{this.props.projects[i].meta}</Card.Meta>
                        <Card.Description>
                            {this.props.projects[i].description}
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        <Button.Group floated='right'>
                            <Button attached='left' inverted color='green'
                                onClick={() => this.setState({ redirect: this.props.projects[i].header })}
                                icon='angle double right' size='large' />
                            <Button attached='right' inverted color='red' onClick={() => this.props.remove(i)}
                                icon='trash outline' size='large' />
                        </Button.Group>
                    </Card.Content>
                </Card>
            );
        return (projs);
    }

    render() {
        if (this.state.redirect) return (<Redirect to={'/projects/' + this.state.redirect} />);
        if (this.props.projects.length === 0 && !this.props.loading) return (
            <Segment textAlign='center'>
                <Header> No projects found. </Header>
            </Segment>
        );
        return (
            <div>
                <Responsive minWidth={600} >
                    <Dimmer.Dimmable as={Segment} dimmed={this.props.loading}>
                        <Dimmer simple inverted>
                            <Loader />
                        </Dimmer>
                        <Card.Group stackable itemsPerRow='4'>
                            {this.showProjects()}
                        </Card.Group>
                    </Dimmer.Dimmable>
                </Responsive>
                <Responsive maxWidth={599} >
                    <br />
                    <Dimmer.Dimmable as={Container} dimmed={this.props.loading}>
                        <Dimmer simple inverted>
                            <Loader />
                        </Dimmer>
                        {this.props.loading && <Segment />}
                        <Card.Group stackable itemsPerRow='4'>
                            {this.showProjects()}
                        </Card.Group>
                    </Dimmer.Dimmable>
                </Responsive>
            </div>
        );
    }
}