import React from 'react';

export default function showWarning (props) {
    if (props.warning) return (
        <div className="ui warning message">
            <div className="header">
                {props.warning}
            </div>
            Please, try again
        </div>
    );
    return(<div />);
}