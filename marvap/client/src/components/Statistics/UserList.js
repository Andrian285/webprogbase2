import React from 'react';
import { Table, Dimmer, Loader, Segment, Container, Button } from 'semantic-ui-react';
import LocalStorageManager from '../Auth/LocalStorageManager';
import sendData from '../Api/Fetcher';

export default class UserList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: null,
            loading: true,
            pages: 0,
            pagesLoaded: 0
        }
    }

    async componentDidMount() {
        let data = await sendData(null, `/admin/userData`, 'GET', LocalStorageManager.getToken());
        let responseData = await data.json();
        this.setState({
            users: responseData.users,
            pages: responseData.pages,
            pagesLoaded: this.state.pagesLoaded + 1
        }, () => this.setState({ loading: false }));
    }

    showUsers() {
        if (this.state.users === null) return;
        let users = [];
        let lastId = this.state.users.length > 100 ? 100 : this.state.users.length;
        for (let i = 0; i < lastId; i++) {
            users.push(
                <Table.Row key={this.state.users[i].username}>
                    <Table.Cell>{this.state.users[i].name}</Table.Cell>
                    <Table.Cell>{this.state.users[i].username}</Table.Cell>
                    <Table.Cell>{this.state.users[i].email}</Table.Cell>
                </Table.Row>
            );
        }
        return users;
    }

    handleAdd = (user) => this.setState(prevState => ({
        users: [...prevState.users, user]
    }));

    loadMore() {
        this.setState({ loading: true }, async () => {
            let data = await sendData(null, `/admin/userData/?page=` + this.state.pagesLoaded, 'GET', LocalStorageManager.getToken());
            let responseData = await data.json();
            this.setState({ loading: false });
            let status = [500, 400, 401, 403];
            if (!status.includes(data.status)) {
                if (responseData.users.length !== 0) for (let i = 0; i < responseData.users.length; i++) {
                    this.handleAdd(responseData.users[i]);
                }
                this.setState({ pages: responseData.pages, pagesLoaded: this.state.pagesLoaded + 1 });
            }
        });
    }

    render() {
        return (
            <Dimmer.Dimmable as={Segment} dimmed={this.state.loading}>
                <Dimmer simple>
                    <Loader />
                </Dimmer>
                <Table celled inverted selectable textAlign='center'>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Full Name</Table.HeaderCell>
                            <Table.HeaderCell>Username</Table.HeaderCell>
                            <Table.HeaderCell>E-mail</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.showUsers()}
                    </Table.Body>
                </Table>
                {this.state.pagesLoaded < this.state.pages &&
                    <Container fluid textAlign='center'><Button onClick={() => this.loadMore()}>Load More</Button></Container>}
            </Dimmer.Dimmable>
        );
    }
}