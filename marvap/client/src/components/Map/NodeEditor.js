import imgDefault from '../../images/defaultImg.jpg';

export const addNewNode = (graph, selectedId, writtenData, photo) => {
    if (selectedId === undefined && graph.nodes.length > 0) return {message: 'Choose some node'};
    if (!writtenData) return {message: 'Enter some data'};

    let newGraph, nodes = [];
    if (graph.nodes.length > 0) {
        nodes = graph.nodes.slice();
        let newId = nodes[nodes.length - 1].id + 1;
        let selectedIndex = nodes.indexOf(nodes.find(x => x.id === selectedId));
        nodes.push({
            id: newId,
            label: writtenData,
            x: nodes[selectedIndex].x + 300,
            y: nodes[selectedIndex].y + 300,
            image: photo === null ? imgDefault : photo,
            brokenImage: imgDefault
        });

        let edges = graph.edges.slice();
        edges.push({
            from: selectedId,
            to: newId
        });

        newGraph = { nodes, edges };
    } else {
        nodes.push({
            id: 0,
            label: writtenData,
            x: 0,
            y: 0,
            image: photo === null ? imgDefault : photo,
            brokenImage: imgDefault
        });
       newGraph = { nodes, edges: [] };
    }
    return newGraph;
}

export const removeSubTree = (graph, selectedId) => {
    if (selectedId === undefined) return null;
    let nodes = graph.nodes.slice();
    let edges = graph.edges.slice();

    removeNode(selectedId, nodes, edges);
    nodes.splice(nodes.indexOf(nodes.find(x => x.id === selectedId)), 1);

    clearEdges(nodes, edges);

    return { nodes, edges };
}

function removeNode(id, nodes, edges) {
    for (let ind in edges) {
        if (edges[ind].from === id) {
            nodes.splice(nodes.indexOf(nodes.find(x => x.id === edges[ind].to)), 1);
            removeNode(edges[ind].to, nodes, edges);
        }
    }
}

function nodeExists(nodes, edges, ind) {
    return (nodes.indexOf(nodes.find(x => x.id === edges[ind].from)) === -1 || nodes.indexOf(nodes.find(x => x.id === edges[ind].to)) === -1);
}

function clearEdges(nodes, edges) {
    let ind = 0;
    while (ind < edges.length) {
        if (nodeExists(nodes, edges, ind)) {
            edges.splice(ind, 1);
        } else ind++;
    }
}