import ReactDOM from 'react-dom';
import React from 'react';
import './stylesheets/index.css';
import 'semantic-ui-css/semantic.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import SignUp from './pages/SignUp';
import Home from './pages/Home';
import Profile from './pages/Profile';
import Projects from './pages/Projects';
import Map from './pages/Map';
import NotFound from './pages/NotFound';
import Statistics from './pages/Statistics';
import Reducer from './reducers/reducer';

const store = createStore(Reducer);

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/signup" component={SignUp} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/statistics" component={Statistics} />
                <Route exact path="/projects" component={Projects} />
                <Route exact path="/projects/:id" component={Map} />
                <Route component={NotFound} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
