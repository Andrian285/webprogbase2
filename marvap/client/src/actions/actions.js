export const setUsername = username => {
    return {type: 'SET_USERNAME', username};
}

export const setIsAdmin = isAdmin => {
    return {type: 'SET_IS_ADMIN', isAdmin};
}

export const logout = () => {
    return {type: 'LOGOUT'};
}