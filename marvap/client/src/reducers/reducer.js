export default function profleReducer (state = [], action) {
    switch (action.type) {
        case 'SET_USERNAME':
            return {...state, username: action.username};
        case 'SET_IS_ADMIN':
            return {...state, isAdmin: action.isAdmin};
        case 'LOGOUT':
            return {...state, username: undefined, isAdmin: undefined};
        default:
            return state;
    }
}