import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import { Header, Container, Segment, Responsive } from 'semantic-ui-react';
import Warning from '../components/Warning/warning';
import LocalStorageManager from '../components/Auth/LocalStorageManager';
import { Redirect } from 'react-router-dom';
import MainBarAuthorized from '../components/NavBar/MainBarAuthorized';
import InfoEditor from '../components/Profile/InfoEditor';
import AvatarEditor from '../components/Profile/AvatarEditor';
import '../stylesheets/index.css';
import sendData from '../components/Api/Fetcher';
import { setUsername } from '../actions/actions';
import { connect } from 'react-redux';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            loading: true,
            loadingSave: false,
            warning: null
        };
    }

    deparam(query) {
        var pairs, i, keyValuePair, key, value, map = {};
        // remove leading question mark if its there
        if (query.slice(0, 1) === '?') {
            query = query.slice(1);
        }
        if (query !== '') {
            pairs = query.split('&');
            for (i = 0; i < pairs.length; i += 1) {
                keyValuePair = pairs[i].split('=');
                key = decodeURIComponent(keyValuePair[0]);
                value = (keyValuePair.length > 1) ? decodeURIComponent(keyValuePair[1]) : undefined;
                map[key] = value;
            }
        }
        return map;
    }

    sendData = async (info) => {
        let obj, photo;
        if (this.state.avatar)
            photo = `avatar=${new Buffer(this.state.avatar).toString('base64')}`;
            obj = info + `&` + photo;

        let data = await sendData(obj, `/user/profile/upload`, 'POST', LocalStorageManager.getToken());
        let responseData = await data.json();
        let status = [500, 400, 401, 403];
        if (!status.includes(data.status)) {
            this.props.setUsername(this.deparam(info).username);
            this.setState({ redirect: true });
        }
        else this.setState({warning: responseData.message, saveIsLoading: false});
    };

    async componentDidMount() {
        if (!LocalStorageManager.isUserAuthenticated()) return;
        let data = await sendData(null, `/user/profile`, 'GET', LocalStorageManager.getToken());
        let responseData = await data.json();
        //if (!responseData.photo) console.log(responseData.photo);
        //console.log(responseData.photo);
        this.setState({
            name: responseData.name, uname: responseData.username, email: responseData.email,
            avatar: !responseData.photo ? null : atob(responseData.photo)
        }, () => this.setState({ loading: false }));
    }

    onLoad = data => {
        this.setState({ avatar: data, showSave: true })
    };


    render() {
        if (!LocalStorageManager.isUserAuthenticated()) return (<Redirect to='/404' />);
        if (this.state.redirect) return (<Redirect to='/' />);
        return (
            <div>
                <MainBarAuthorized page={this.props.match} />
                <Container text textAlign='justified'>
                    <br />
                    <Warning warning={this.state.warning}/>
                    <Segment>
                        <Header size='huge' textAlign='center'>Profile</Header>
                    </Segment>
                    <Responsive minWidth={720} >
                        <Segment>
                            <div className="ui grid">
                                <div className="seven wide column">
                                    <AvatarEditor loading={this.state.loading} avatar={this.state.avatar}
                                        onLoad={this.onLoad} />
                                </div>
                                <div className="eight wide column">
                                    <InfoEditor name={this.state.name} uname={this.state.uname} email={this.state.email}
                                        showSave={this.state.showSave} sendData={this.sendData}
                                        setStateSaveLoading={(e) => this.setState({saveIsLoading: true})} saveIsLoading={this.state.saveIsLoading}/>
                                </div>
                            </div>
                        </Segment>
                    </Responsive>
                    <Responsive maxWidth={719} >
                        <Segment>
                            <Container textAlign='center'>
                                <AvatarEditor loading={this.state.loading} avatar={this.state.avatar}
                                    onLoad={this.onLoad} />
                            </Container>
                            <InfoEditor name={this.state.name} uname={this.state.uname} email={this.state.email}
                                showSave={this.state.showSave} sendData={this.sendData}
                                setStateSaveLoading={(e) => this.setState({saveIsLoading: true})} saveIsLoading={this.state.saveIsLoading}/>
                        </Segment>
                    </Responsive>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    username: state.username
});

const mapDispatchToProps = {
    setUsername
};
const ProfilePage = connect(mapStateToProps, mapDispatchToProps)(Profile);

export default ProfilePage;