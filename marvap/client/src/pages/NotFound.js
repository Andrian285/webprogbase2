import React from 'react';
import '../stylesheets/index.css';
import 'semantic-ui-css/semantic.min.css';
import {Header, Container} from 'semantic-ui-react';
import MainBar from '../components/NavBar/MainBar';
import MainBarAuthorized from '../components/NavBar/MainBarAuthorized';
import LocalStorageManager from '../components/Auth/LocalStorageManager';

export default class Home extends React.Component {
    static renderBar() {
        if (LocalStorageManager.isUserAuthenticated()) return <MainBarAuthorized page={'/404'}/>;
        else return <MainBar page={'/404'}/>;
    }

    render() {
        return (
            <div>
                {Home.renderBar()}
                <Container text textAlign='justified'>
                    <br/>
                    <Header size='huge' textAlign='center' attached='top'>Not Found</Header>
                </Container>
            </div>
        );
    }
}
