import React from 'react';
import '../stylesheets/index.css';
import 'semantic-ui-css/semantic.min.css';
import {Header, Container, Segment} from 'semantic-ui-react';
import MainBar from '../components/NavBar/MainBar';
import {Redirect} from 'react-router-dom';
import Auth from '../components/Auth/Auth';
import LocalStorageManager from '../components/Auth/LocalStorageManager';

export default class SignUp extends React.Component {
    render() {
        if (LocalStorageManager.isUserAuthenticated()) return (<Redirect to='/'/>);
        return (
            <div>
                <MainBar page={this.props.match}/>
                <Container text textAlign='justified'>
                    <br/>
                    <Segment>
                        <Header size='huge' textAlign='center'>Sign Up</Header>
                    </Segment>
                    <Segment textAlign='center'>
                        <Auth name username password confirm email submit sendTo='/auth/signup' page={this.props.match}/>
                    </Segment>
                </Container>
            </div>
        );
    }
}