import React from 'react';
import '../stylesheets/index.css';
import 'semantic-ui-css/semantic.min.css';
import { Container, Segment, Image, Divider, Button, Responsive } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import logo from '../images/logo.png';
import MainBar from '../components/NavBar/MainBar';
import MainBarAuthorized from '../components/NavBar/MainBarAuthorized';
import LocalStorageManager from '../components/Auth/LocalStorageManager';

export default class Home extends React.Component {
    renderBar() {
        if (LocalStorageManager.isUserAuthenticated()) return <MainBarAuthorized page={this.props.match} />;
        else return <MainBar page={this.props.match} />;
    }

    static getStarted() {
        if (!LocalStorageManager.isUserAuthenticated()) return (
            <Container textAlign='center'>
                <NavLink to='/signup'>
                    <Button inverted color='green' size='massive'>Get Started</Button>
                </NavLink>
            </Container>
        ); else return (
            <Container textAlign='center'>
                <NavLink to='/projects'>
                    <Button inverted color='green' size='massive'>Get Started</Button>
                </NavLink>
            </Container>
        );
    }

    render() {
        return (
            <div>
                {this.renderBar()}
                <Segment inverted color='grey' attached='bottom' vertical className='mainPageSegment'>
                    <div className='verticallyAlignedInfo'>
                        <h1 className='bigtext'>Marvap</h1>
                        <h2 className='underbigtext'> The best way to create Mindmaps.</h2>
                        {Home.getStarted()}
                    </div>
                </Segment>
                <Responsive minWidth={1400} >
                    <Container fluid className='mainDescription'>
                        <div className="ui grid">
                            <div className="nine wide column">
                                <Divider horizontal>About</Divider>
                                <Container text>
                                    <span className='descriptionMain'>Marvap is an online instrument that helps you to create and work with mind maps
                                    if you’d like to generate, display, structurize, or classify any ideas. It makes your learning, organizing,
                                    problem solving, decision making, and writing documents easier. In contrast to the usual brainstorming, during which a
                                    plurality of disordered ideas is obtained and then subsequently arranged, the use of mind maps promotes the formation of
                                    network structures from the very beginning. Maps can also be the documentation of the brainstorming outcomes.</span>
                                </Container>
                            </div>
                            <div className="seven wide column">
                                <Image src={logo} className='logoMain' />
                            </div>
                        </div>
                    </Container>
                </Responsive>
                <Responsive maxWidth={1399} >
                    <Container fluid className='mainDescription'>
                        <Divider horizontal>About</Divider>
                        <Container text textAlign='justified'>
                            <span className='descriptionMain'>Marvap is an online instrument that helps you to create and work with mind maps
                            if you’d like to generate, display, structurize, or classify any ideas. It makes your learning, organizing,
                            problem solving, decision making, and writing documents easier. In contrast to the usual brainstorming, during which a
                            plurality of disordered ideas is obtained and then subsequently arranged, the use of mind maps promotes the formation of
                            network structures from the very beginning. Maps can also be the documentation of the brainstorming outcomes.</span>
                        </Container>
                        <br />
                        <Image src={logo} centered className='logoMain' />
                    </Container>
                </Responsive>
            </div>
        );
    }
}