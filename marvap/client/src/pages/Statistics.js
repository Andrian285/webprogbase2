import React from 'react';
import { Statistic, Container, Segment, Button, Divider, Dimmer, Loader, Icon, Responsive } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import LocalStorageManager from '../components/Auth/LocalStorageManager';
import MainBarAuthorized from '../components/NavBar/MainBarAuthorized';
import UserList from '../components/Statistics/UserList';
import sendData from '../components/Api/Fetcher';
import { connect } from 'react-redux';

class Statistics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: null,
            projects: null,
            loading: true,
            activeMore: true,
            redirect: null
        }
    }

    async componentDidMount() {
        if (!LocalStorageManager.isAdmin()) return;
        let data = await sendData(null, `/admin/userStatistics`, 'GET', LocalStorageManager.getToken());
        if (data.status === 401 || data.status === 403) this.setState({redirect: '/404'});
        let responseData = await data.json();
        this.setState({
            users: responseData.users,
            projects: responseData.projects
        }, () => this.setState({ loading: false }));
    }

    userData() {
        if (this.state.activeMore) return (
            <Segment textAlign='center'>
                <Button circular animated='fade' size='huge' onClick={() => this.setState({ activeMore: false })}>
                    <Button.Content visible>
                        More
                    </Button.Content>
                    <Button.Content hidden>
                        <Icon name='users' />
                    </Button.Content>
                </Button>
            </Segment>
        );
        else return (<UserList />);
    }

    render() {
        if (!LocalStorageManager.isAdmin()) return <Redirect to='/404' />;
        if (this.state.redirect) return <Redirect to={this.state.redirect} />;
        return (
            <div>
                <MainBarAuthorized page={this.props.match} />
                <Container text textAlign='justified'>
                    <br />
                    <Segment inverted textAlign='center'>
                        <Dimmer active={this.state.loading}>
                            <Loader />
                        </Dimmer>
                        <Responsive minWidth={600} >
                            <Statistic.Group inverted widths='two'>
                                <Statistic>
                                    <Statistic.Value>{this.state.users}</Statistic.Value>
                                    <Statistic.Label>Users</Statistic.Label>
                                </Statistic>
                                <Statistic>
                                    <Statistic.Value>{this.state.projects}</Statistic.Value>
                                    <Statistic.Label>Created projects</Statistic.Label>
                                </Statistic>
                            </Statistic.Group>
                        </Responsive>

                        <Responsive maxWidth={599} >
                            <Statistic.Group inverted widths='one'>
                                <Statistic>
                                    <Statistic.Value>{this.state.users}</Statistic.Value>
                                    <Statistic.Label>Users</Statistic.Label>
                                </Statistic>
                                <Statistic>
                                    <Statistic.Value>{this.state.projects}</Statistic.Value>
                                    <Statistic.Label>Created projects</Statistic.Label>
                                </Statistic>
                            </Statistic.Group>
                        </Responsive>
                    </Segment>
                    <Divider />
                    {this.userData()}
                </Container>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    isAdmin: state.isAdmin
});

const StatisticsComponent = connect(mapStateToProps)(Statistics);

export default StatisticsComponent;