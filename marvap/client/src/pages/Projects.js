import React from 'react';
import '../stylesheets/index.css';
import 'semantic-ui-css/semantic.min.css';
import { Modal, Segment, Button, Input, Header, Icon, Label, Container } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import MainBarAuthorized from '../components/NavBar/MainBarAuthorized';
import LocalStorageManager from '../components/Auth/LocalStorageManager';
import Warning from '../components/Warning/warning';
import ProjectForm from '../components/Projects/ProjectForm';
import ProjectList from '../components/Projects/ProjectList';
import sendData from '../components/Api/Fetcher';

export default class Projects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            filteredProjects: [],
            openAdd: false,
            openRemove: false,
            redirect: null,
            trash: false,
            removeId: null,
            loadingRemove: false,
            loadingProjects: true,
            warning: null,
            searchText: null,
            pages: 0,
            pagesLoaded: 0
        };
    }

    showAdd = () => this.setState({ openAdd: true });
    closeAdd = () => this.setState({ openAdd: false });
    showRemove = () => this.setState({ openRemove: true });
    closeRemove = () => this.setState({ openRemove: false });

    async componentDidMount() {
        if (!LocalStorageManager.isUserAuthenticated()) return;
        let data = await sendData(null, `/user/projects`, 'GET', LocalStorageManager.getToken());
        let responseData = await data.json();
        this.setState({
            pages: responseData.pages,
            pagesLoaded: this.state.pagesLoaded + 1,
            loadingProjects: false
        });
        if (responseData.projects.length !== 0) for (let i = 0; i < responseData.projects.length; i++) {
            this.handleAdd(responseData.projects[i].header, responseData.projects[i].meta, responseData.projects[i].description);
        }
    }

    handleAdd = (header, meta, description) => this.setState(prevState => ({
        projects: [...prevState.projects, {
            header: header,
            meta: meta,
            description: description
        }]
    }), () => this.setState({ filteredProjects: this.state.projects }));

    handleAddFirst = (header, meta, description) => this.setState(prevState => ({
        projects: [{
            header: header,
            meta: meta,
            description: description
        }, ...prevState.projects]
    }));

    remove = async () => {
        let id = this.state.removeId;
        let tmpArr = this.state.projects.slice();
        tmpArr.splice(id, 1);
        //console.log(this.state.projects[id].header);

        let data = await sendData(`name=${this.state.projects[id].header}`, `/user/projects/remove`, 'POST', LocalStorageManager.getToken());
        let status = [500, 400, 401, 403];
        if (!status.includes(data.status)) {
            this.setState({ loadingRemove: false });
            this.setState({ projects: tmpArr, filteredProjects: tmpArr });
        }
        this.setState({ removeId: null });
    };

    addProjectOnClient = async (data, header, meta, description) => {
        let status = [500, 400, 401, 403];
        if (status.includes(data.status)) {
            let responseData = await data.json();
            this.setState({ warning: responseData.message });
        }
        else {
            this.handleAddFirst(header, meta, description);
            this.closeAdd();
        }
    };

    async loadProjects() {
        let filterText = this.state.searchText ? this.state.searchText : '';
        let data = await sendData(null, `/user/projects/?page=` + this.state.pagesLoaded + 
        `&search=` + filterText, 'GET', LocalStorageManager.getToken());
        let responseData = await data.json();
        this.setState({ loadingProjects: false });
        let status = [500, 400, 401, 403];
        if (!status.includes(data.status)) {
            if (responseData.projects.length !== 0) for (let i = 0; i < responseData.projects.length; i++) {
                this.handleAdd(responseData.projects[i].header, responseData.projects[i].meta, responseData.projects[i].description);
            }
            this.setState({ pages: responseData.pages, pagesLoaded: this.state.pagesLoaded + 1 });
        } else console.log(responseData.message);
    }

    searchProjects() {
        this.setState({ loadingProjects: true, pagesLoaded: 0, projects: [] }, () => this.loadProjects());
    }

    loadMore() {
        this.setState({ loadingProjects: true }, () => this.loadProjects());
    }

    render() {
        if (!LocalStorageManager.isUserAuthenticated()) return (<Redirect to='/404' />);
        if (this.state.redirect) return (<Redirect to={'/projects/' + this.state.redirect} />);
        return (
            <div>
                <MainBarAuthorized page={this.props.match} />
                <Segment textAlign='right'>
                    <Input onChange={(e) => this.setState({ searchText: e.target.value })} placeholder='search...' type='text' />
                    <Label as='a' size='large' tag onClick={() => this.searchProjects()}>Find</Label>
                </Segment>
                <ProjectList projects={this.state.projects} loading={this.state.loadingProjects}
                    remove={(id) => this.setState({ removeId: id }, () => this.showRemove())} />
                {this.state.pagesLoaded < this.state.pages &&
                    <Container fluid textAlign='right'><Button onClick={() => this.loadMore()}>Load More</Button></Container>}
                <Segment circular floated='right'>
                    <Button positive disabled={false} onClick={() => this.showAdd()} icon='plus' />
                </Segment>


                <Modal size='mini' open={this.state.openAdd} onClose={this.closeAdd}>
                    <Warning warning={this.state.warning} />
                    <Modal.Header>Enter some parameters for your project</Modal.Header>
                    <Modal.Content image>
                        <ProjectForm addProjectOnClient={this.addProjectOnClient} />
                    </Modal.Content>
                </Modal>

                <Modal open={this.state.openRemove} basic size='small' onClose={this.closeRemove}>
                    <Header icon='trash outline' content='Remove Project' />
                    <Modal.Content>
                        <p>Are you sure?</p>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button basic color='red' inverted onClick={() => this.closeRemove()}>
                            <Icon name='remove' /> No
                        </Button>
                        <Button color='green' inverted onClick={() => { this.remove(); this.closeRemove(); }}>
                            <Icon name='checkmark' /> Yes
                        </Button>
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }
}
