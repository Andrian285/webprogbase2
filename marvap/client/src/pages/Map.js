import React from 'react';
import '../stylesheets/index.css';
import 'semantic-ui-css/semantic.min.css';
import { Label, Button, Card, Segment, TextArea, Form, Input, Modal, Header, Icon, Responsive } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import MainBarAuthorized from '../components/NavBar/MainBarAuthorized';
import LocalStorageManager from '../components/Auth/LocalStorageManager';
import sendData from '../components/Api/Fetcher';
import Graph from 'react-graph-vis';
import { addNewNode, removeSubTree } from '../components/Map/NodeEditor';
import Alert from 'react-s-alert';
import HotKey from 'react-shortcut';
import imgDefault from '../images/imgDefault.png';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';

const nodeTextMaxLength = 20;

const options = {
    autoResize: true,
    layout: {
        hierarchical: false
    },
    nodes: {
        shape: 'circularImage',
        borderWidth: 3
    },
    edges: {
        color: "#000000"
    },
    physics: {
        barnesHut: {
            springConstant: 0
        },
        timestep: 0
    }
};

export default class Map extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            graph: {
                nodes: [],
                edges: []
            },
            selectedId: undefined,
            writtenData: null,
            inputFileVisible: false,
            photo: null,
            infoModal: false,
            editNodeModal: false,
            editData: null,
            editInputFileVisible: false
        }
        this.events.select = this.events.select.bind(this);
        this.events.dragEnd = this.events.dragEnd.bind(this);
        this.events.deselectNode = this.events.deselectNode.bind(this);
        this.events.doubleClick = this.events.doubleClick.bind(this);
        this.loadPhoto = this.loadPhoto.bind(this);
        //this.editNode = this.editNode.bind(this);
    }

    async componentDidMount() {
        if (!LocalStorageManager.isUserAuthenticated()) return;
        let data = await sendData(null, `/user/projects/` + this.props.match.params.id, 'GET', LocalStorageManager.getToken());
        let responseData = await data.json();
        let status = [500, 400, 401, 403];
        if (status.includes(data.status)) return;
        let stateNodes = responseData.project.nodes.slice();

        for (let node of stateNodes) {
            node.image = atob(node.image);
        }
        let graph = { nodes: stateNodes, edges: responseData.project.edges };
        this.setState({ graph });
    }

    getNewPositionedNode(newX, newY, index) {
        let nodes = this.state.graph.nodes.slice();
        let ind = nodes.indexOf(nodes.find(x => x.id === index));
        nodes[ind].x = newX; nodes[ind].y = newY;
        return nodes;
    }

    events = {
        select: function (event) {
            let { nodes } = event;
            if (nodes[0] !== undefined) this.setState({ selectedId: nodes[0] });
        },
        dragEnd: function (event) {
            let { nodes, pointer } = event;

            if (nodes[0] !== undefined) {
                let newNodes = this.state.graph.nodes.slice();
                newNodes = this.getNewPositionedNode(pointer.canvas.x, pointer.canvas.y, nodes[0]);
                let graph = { nodes: newNodes, edges: this.state.graph.edges.slice() };

                this.setState({ graph, selectedId: nodes[0] }, () => this.save());
            }
        },
        deselectNode: function (event) {
            this.setState({ selectedId: undefined });
        },
        doubleClick: function (event) {
            let { nodes } = event;
            if (nodes[0] !== undefined) {
                let ind = this.state.graph.nodes.indexOf(this.state.graph.nodes.find(x => x.id === nodes[0]));
                this.setState({ editNodeModal: true, editData: this.state.graph.nodes[ind].label });
            }
        }
    };

    async save(withPhoto) {
        let basicNodes;
        if (!withPhoto) basicNodes = this.state.graph.nodes.map(node => { return { id: node.id, label: node.label, x: node.x, y: node.y } });
        else {
            basicNodes = this.state.graph.nodes.map(node => { return { id: node.id, label: node.label, x: node.x, y: node.y, image: btoa(node.image) } });
        }
        //console.log(JSON.stringify(this.state.graph.nodes, null, 4));

        let nodes = JSON.stringify(basicNodes), edges = JSON.stringify(this.state.graph.edges);
        let body = `nodes=${nodes}&edges=${edges}`;
        let data = await sendData(body, `/user/projects/` + this.props.match.params.id + `/save`, 'POST', LocalStorageManager.getToken());
        let status = [500, 400, 401, 403];
        if (status.includes(data.status)) console.log('oops, smth went wrong!');
    }

    addNewNode() {
        let graph = addNewNode(this.state.graph, this.state.selectedId, this.state.writtenData, this.state.photo);
        if (graph.message) return Alert.error(graph.message, {
            position: 'bottom-left',
            effect: 'slide',
            timeout: 4000
        });
        this.setState({ photo: null, inputFileVisible: false });
        this.setState({ graph }, () => this.save(true));
    }

    removeSubTree() {
        let graph = removeSubTree(this.state.graph, this.state.selectedId);
        if (!graph) return Alert.error('Choose some node', {
            position: 'bottom-left',
            effect: 'slide',
            timeout: 4000
        });
        this.setState({ graph, selectedId: undefined }, () => this.save());
    }

    async loadPhoto(evt) {
        const reader = new FileReader();
        let photo = evt.target.files[0];
        reader.onloadend = () => {
            if (photo.size > 900000 || (photo.type !== 'image/jpeg' && photo.type !== 'image/png'))
                return Alert.error('Incorrect file type (it should be an image (jpeg/png) with size less than 900KB)', {
                    position: 'bottom-left',
                    effect: 'slide',
                    timeout: 5000
                });
            else {
                this.setState({ photo: reader.result });
            }
        };
        try {
            reader.readAsDataURL(photo);
        } catch (e) {
            //loading canceled
        }
    }

    editNode() {
        if (!this.state.editData) {
            this.setState({ editNodeModal: false });
            return Alert.error('Node should contain some data', {
                position: 'bottom-left',
                effect: 'slide',
                timeout: 5000
            });
        }
        let ind = this.state.graph.nodes.indexOf(this.state.graph.nodes.find(x => x.id === this.state.selectedId));
        let nodesCopy = this.state.graph.nodes.map(node => {
            if (node.id === this.state.selectedId) return {
                id: this.state.selectedId,
                label: this.state.editData,
                x: this.state.graph.nodes[ind].x,
                y: this.state.graph.nodes[ind].y,
                image: this.state.photo ? this.state.photo : this.state.graph.nodes[ind].image,
                brokenImage: imgDefault
            }
            return node;
        });

        let graph = { nodes: nodesCopy, edges: this.state.graph.edges.slice() };
        this.setState({ graph, editNodeModal: false, photo: null }, () => this.save(true));
    }

    handleTextChange(e) {
        let text = e.target.value.slice();
        for (let i = 0; i < text.length; i++) {
            if ((i + 1) % nodeTextMaxLength === 0) {
                let tmpText = text.slice(0, i) + '\n' + text.slice(i);
                text = tmpText.slice();
            }
        }
        this.setState({ writtenData: text });
    }

    render() {
        if (!LocalStorageManager.isUserAuthenticated()) return (<Redirect to='/' />);
        return (
            <div className="map">
                <div className='navbar'><MainBarAuthorized page={this.props.match} /></div>
                <Responsive minWidth={600}>
                    <Segment className='editorBox' compact basic>
                        <Card>
                            <Card.Content>
                                <Card.Description>
                                    <Form>
                                        <Label size='large' color='teal' ribbon>Data:</Label>
                                        <br />
                                        <TextArea autoHeight onChange={(e) => this.handleTextChange(e)} maxLength='200' />
                                        {this.state.inputFileVisible ? (
                                            <Input type='file' onChange={this.loadPhoto} accept="image/x-png,image/gif,image/jpeg" />
                                        ) : (
                                                <div>
                                                    <Label as='a' size='large' color='teal' ribbon
                                                        onClick={() => this.setState({ inputFileVisible: true })}>Photo (optional)</Label>
                                                </div>
                                            )}

                                    </Form>
                                </Card.Description>
                            </Card.Content>
                            <Card.Content extra>
                                <div className='ui two buttons'>
                                    <Button basic color='green' onClick={() => this.addNewNode()}>Add</Button>
                                    <Button basic color='red' onClick={() => this.removeSubTree()}>Remove</Button>
                                </div>
                            </Card.Content>
                        </Card>
                        <label style={{ color: '#0ea4a9' }}>Press Shift+H to see help menu</label>
                    </Segment>
                    <Segment compact className='userEditor'>
                        <Button>kek</Button>
                    </Segment>
                </Responsive>
                <div className='fix'><Graph graph={this.state.graph} options={options} events={this.events} /></div>

                <Alert stack={{ limit: 3 }} />
                <HotKey
                    keys={['shift', 'h']}
                    onKeysCoincide={() => this.setState({ infoModal: true })}
                />

                <Modal
                    open={this.state.infoModal}
                    onClose={() => this.setState({ infoModal: false })}
                    basic
                    size='small'
                >
                    <Header icon='info' content='Help box' />
                    <Modal.Content>
                        <h3>Welcome to Marvap MindMap. Here you can create, edit and share your mind maps. To create
                            new node, enter some data in the textfield 'Data' and click on 'Add' button. You may click on 'Photo (optional)'
                            to choose some photo for your node.</h3>
                        <h3>To delete node, select it and click 'Remove' button. To edit data of the node, double click it.</h3>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='green' onClick={() => this.setState({ infoModal: false })} inverted>
                            <Icon name='checkmark' /> Got it
                        </Button>
                    </Modal.Actions>
                </Modal>

                <Modal size='small' open={this.state.editNodeModal} onClose={() => this.setState({ editNodeModal: false })}>
                    <Modal.Header>
                        Edit node data
                    </Modal.Header>
                    <Modal.Content>
                        <Form>
                            <Form.Field>
                                <label>Data:</label>
                                <TextArea autoHeight value={this.state.editData} onChange={(e) => this.setState({ editData: e.target.value })} />
                            </Form.Field>
                            {this.state.editInputFileVisible ? (
                                <Input type='file' onChange={this.loadPhoto} accept="image/x-png,image/gif,image/jpeg" />
                            ) : (
                                    <Form.Field>
                                        <Button onClick={() => this.setState({ editInputFileVisible: true })}>Change Photo</Button>
                                    </Form.Field>
                                )}
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button negative icon='cancel' labelPosition='right' content='Cancel' onClick={() => this.setState({ editNodeModal: false })} />
                        <Button positive icon='checkmark' labelPosition='right' content='Save'
                            onClick={() => this.editNode()} />
                    </Modal.Actions>
                </Modal>
            </div>
        );
    }
}